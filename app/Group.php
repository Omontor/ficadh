<?php

namespace App;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users(){
        return $this->hasMany(User::class);
    }

}
