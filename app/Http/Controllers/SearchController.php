<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Municipio;
use App\Entidad;
  
class SearchController extends Controller
{
 
public function getStates($id) 
{        
        $states = Municipio::where("entidad_id",$id)->pluck("name","id");
        return json_encode($states);
}
}