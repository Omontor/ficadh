<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    public function municipio(){
        return $this->hasMany(Municipio::class);
    }

}
