<footer class="app-footer">
        <div class="d-block w-100 text-center"><strong> {{config('app.name')}}  {{ date('Y') }} </strong> Todos los Derechos Reservados <p class="float-right mb-0">{{  now()->toDateTimeString()}}</p></div>

</footer>
