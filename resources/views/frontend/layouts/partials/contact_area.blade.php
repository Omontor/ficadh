<section id="contact-area" class="contact-area-section backgroud-style">
    <div class="container">
        <div class="contact-area-content">
            <div class="row">
                @if(config('contact_data') != "")
                    @php
                        $contact_data = contact_data(config('contact_data'));
                    @endphp
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6">
                        <div class="contact-center-content ">
                            <div class="section-title  mb45 headline text-center">
                                <span class="subtitle ml42  text-uppercase">FICADH</span>
                                <h2><span>Contacto</span></h2>
                                <p>
                                    {{ $contact_data["short_text"]["value"] }}
                                </p>
                            </div>

                            <div class="contact-address">
                                

                                @if(($contact_data["primary_email"]["status"] == 1) || ($contact_data["secondary_email"]["status"] == 1))

                                    <div class="contact-address-details">
                                        <div class="address-icon relative-position text-center float-left">
                                            <i class="fas fa-envelope"></i>
                                        </div>
                                        <div class="address-details ul-li-block">
                                            <ul>
                                                @if($contact_data["primary_email"]["status"] == 1)
                                                    <li>
                                                        <span>email: </span>contacto@ficadh.org
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="genius-btn btn-block gradient-bg text-center text-uppercase ul-li-block bold-font ">
                            <a href="{{route('contact')}}">Contacto <i class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                         <div class="col-md-3">
                    </div>
                    <!-- Quitar el mapa 
                    @if($contact_data["location_on_map"]["status"] == 1)
                        <div class="col-md-6">
                            <div id="contact-map" class="contact-map-section">
                                {!! $contact_data["location_on_map"]["value"] !!}
                            </div>
                        </div>
                    @endif
                    -->
                @else
                    <h4>@lang('labels.general.no_data_available')</h4>
                @endif
            </div>
        </div>
    </div>
</section>
