<?php

use Illuminate\Database\Seeder;
Use App\Models\Category;
Use App\Models\Course;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run()
    {

  
        $faker = \Faker\Factory::create();


       $category = new Category;
       $category->name = 'Categoría de prueba';
       $category->icon ='fas fa-bomb';
       $category->status = 1;
       $category->slug = 'testcategory';
       $category->save();

       $course = new Course;
       $course->category_id = 1;
       $course->title = 'Curso de prueba';
       $course->slug = 'testcourse';
       $course->description =  $faker->paragraph;
       $course->price = 20;
       $course->course_image = null;
       $course->start_date = '2020-12-26';
       $course->featured = 1;
       $course->trending = 1;
       $course->popular =1;
       $course->meta_title =  $course->title; 
       $course->meta_description = $course->description;
       $course->meta_keywords = 'test';
       $course->published = 1;
       $course->free =1;
       $course->expire_at = '2020-12-29';
       $course->save();


    }
}
