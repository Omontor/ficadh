<?php

use Illuminate\Database\Seeder;

class LocaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = [

            [
                'name' => 'Spanish',
                'short_name' => 'es',
                'display_type' => 'ltr',
                'is_default' => 0,

            ]

        ];

        foreach ($locales as $item) {
            \App\Models\Locale::firstOrCreate([
                'name' => $item['name'],
                'short_name' => $item['short_name'],
                'display_type' => $item['display_type'],
                'is_default' => $item['is_default']]);
        }



    }
}
