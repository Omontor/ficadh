<?php

use Illuminate\Database\Seeder;
use App\Locality;

class LocalitySeeder extends Seeder
{

	public function run()
{
    DB::table('localities')->delete();
    $json = File::get("database/data.json");
    $data = json_decode($json);
    foreach ($data as $obj) {
        Locality::create(array(
		'idEstado' => $obj->idEstado,
		'estado' => $obj->estado,
		'idMunicipio' => $obj->idMunicipio,
		'municipio' => $obj->municipio,
		'ciudad' => $obj->ciudad,
		'zona' => $obj->zona,
		'cp'  => $obj->cp,
		'asentamiento'  => $obj->asentamiento,
		'tipo'  => $obj->tipo,
        ));
    }
}

 

    
}
