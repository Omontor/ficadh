<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = new Group();
        $group->name = 'Sin Asignar';
        $group->save();

        $group = new Group();
        $group->name = 'Grupo A';
        $group->save();

        $group = new Group();
        $group->name = 'Grupo B';
        $group->save();

        $group = new Group();
        $group->name = 'Grupo C';
        $group->save();


    }
}
